# filestore-groupe2

## Génération de vignettes et extraction de méta données
Il s'agit de proposer un service de génération de vignettes pour les éléments du store.
Le service sera accessible via une évolution de l'API. Il faut penser à la façon d'intégrer l'usage des
vignettes dans la UI et proposer également des solutions d'amélioration des performances (cache HTTP).

## Versions
- Maven 3.5.4
- JDK 1.8
- Docker 19.03.5-ce
- Docker API 1.40

**/!\ Il est impératif d'utiliser Maven 3.5.\* et Java 8 /!\\**

## Installation et exécution
La première étape est de lancer le serevur Keycloak dans un conteneur Docker.
Pour cela, exécuter le script `/keycloak/keycloak-setup.sh` :
```
$ cd keycloak
$ ./keycloak-setup.sh
Keycloak directory: '/home/wk/.keycloak'
42016367d5583f1bd0b3f28b7f8895792c041e7b610e7b0b022822453c6ac837
Waiting for Keycloak server to start.........
Keycloak server set up successfully!
```

Une fois le serveur Keycloak lancé, exécuter le script `/run.sh` :
```
$ ./run.sh
```

Ce dernier se charge d'exécuter les commandes Maven pour :
- Générer les artefacts
- Lancer la migration Liquibase
- Peupler la base de données avec le script SQL des utilisateurs
- Exécuter le fichier JAR généré

## Fichiers supportés
- Office (doc, docx, xls, xlsx, ppt, pptx)
- OpenOffice (tous)
- Text (txt, pdf, rtf, html)
- Image (jpg, png, bmp, gif)
- AutoCad (dwg)
- MP3 (utilise la vignette de l'album)
- MP4 (génère un gif, ou presque)

Les fichiers MS Office/OpenOffice nécessitent l'installation d'OpenOffice sur le serveur.

Les fichiers MP4 nécessitent l'installation de FFMPEG sur le serveur.

## Auteurs
- Jérémy Thomas
- Lionel Legrand
- Loïc Pierson
- Pierre Duclou
