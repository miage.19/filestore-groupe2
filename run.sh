#!/bin/sh

export TEMP_DIR="$HOME/.filestore/binarydata/thumbnails/"
export THUMB_HEIGHT=100
export THUMB_WIDTH=100

docker container start keycloak_fs  >/dev/null 2>&1

if [[ $? -eq 1 ]]; then
    echo 'Keycloak container is not running.'
    echo 'Please run the "keycloak-setup.sh" script first.'
    exit 1
fi

echo 'Packaging Maven project...'
mvn clean package -DskipTests

echo 'Executing Liquibase migration...'
mvn liquibase:migrate

echo 'Seeding H2 database...'
java \
    -cp ~/.m2/repository/com/h2database/h2/1.4.196/h2-1.4.196.jar org.h2.tools.RunScript \
    -url jdbc:h2:"$HOME"/.filestore/filestore \
    -user sa \
    -password sa \
    -script ./src/main/resources/scripts/users.sql \
    -showResults

echo 'Running JAR artifact...'
java \
    -jar ./target/*-1.0.0-SNAPSHOT-thorntail.jar \
    -Dswarm.port.offset=100 \
    -Dfilestore.instance.home="$HOME/.filestore"
