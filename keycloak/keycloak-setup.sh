#!/bin/sh

PORT=8080
KEYCLOAK_HOME="$HOME/.keycloak"
KEYCLOAK_USER=admin
KEYCLOAK_PASSWORD=admin
KEYCLOAK_EXPORT="realm-export.json"
DIR="$(cd "$(dirname "$0")" >/dev/null 2>&1 && pwd)"

[[ ! -d "$KEYCLOAK_HOME"  ]] && mkdir "$KEYCLOAK_HOME"

echo "Keycloak directory: '$KEYCLOAK_HOME'"

docker container start keycloak_fs  >/dev/null 2>&1 || docker container run \
    -d \
    --name keycloak_fs \
    -p $PORT:$PORT \
    -v "$KEYCLOAK_HOME":/opt/jboss/keycloak/standalone/data \
    -v "$DIR/$KEYCLOAK_EXPORT":"/tmp/$KEYCLOAK_EXPORT" \
    -e KEYCLOAK_USER=$KEYCLOAK_USER \
    -e KEYCLOAK_PASSWORD=$KEYCLOAK_PASSWORD \
    -e KEYCLOAK_IMPORT="/tmp/$KEYCLOAK_EXPORT" \
    jboss/keycloak

printf 'Waiting for Keycloak server to start'

until $(curl --output /dev/null --silent --head --fail http://localhost:$PORT); do
    printf '.'
    sleep 1
done

echo -e "\nKeycloak server set up successfully!"
