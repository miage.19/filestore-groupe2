package fr.miage.filestore.store;

import java.io.InputStream;
import java.nio.file.Path;

public interface BinaryStoreService {

    boolean exists(String key) throws BinaryStoreServiceException;

    String type(String key, String name) throws BinaryStoreServiceException, BinaryStreamNotFoundException;

    String put(InputStream is) throws BinaryStoreServiceException;

    long size(String key) throws BinaryStoreServiceException, BinaryStreamNotFoundException;

    InputStream get(String key) throws BinaryStoreServiceException, BinaryStreamNotFoundException;

    Path getPath(String key) throws BinaryStreamNotFoundException;

    void delete(String key) throws BinaryStoreServiceException, BinaryStreamNotFoundException;

}
