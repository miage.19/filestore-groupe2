package fr.miage.filestore.file.tree;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;
import java.util.stream.Collectors;

public class Tree extends TreeEntry {

    List<TreeEntry> entries;

    public Tree(String name, String key) {
        this.setId(Base58.encodeUUID(UUID.randomUUID()));
        this.setName(name);
        this.setKey(key);
        entries = new ArrayList<>();
    }

    public void addEntry(TreeEntry entry) {

    }

    public void removeEntry(String id) {

    }

    public void setContent(String content) {

    }

    public String getContent() {
        return entries.stream().map(TreeEntry::serialize).collect(Collectors.joining("\r\n"));
    }

    @Override
    public String serialize() {
        return "tree " + getId() + " " + getKey() + " " + getName();
    }
}
