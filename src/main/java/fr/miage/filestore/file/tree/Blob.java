package fr.miage.filestore.file.tree;

public class Blob extends TreeEntry {

    @Override
    public String serialize() {
        return "blob " + getId() + " " + getKey() + " " + getName();
    }
}