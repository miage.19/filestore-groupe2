package fr.miage.filestore.file;

public interface FileServiceMetrics {

    int getNbUploads();

    int getNbDownloads();

    int getLatestUploads();

    int getLatestDownloads();
}
