package fr.miage.filestore.thumbnail;

public class ThumbnailCreationFailedException extends RuntimeException {

    public ThumbnailCreationFailedException(String message) {
        super(message);
    }

}
