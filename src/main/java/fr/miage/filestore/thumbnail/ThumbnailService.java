package fr.miage.filestore.thumbnail;

public interface ThumbnailService {

    void create(String key);

    String toBase64(String key) throws ThumbnailBase64EncodingException;

}
