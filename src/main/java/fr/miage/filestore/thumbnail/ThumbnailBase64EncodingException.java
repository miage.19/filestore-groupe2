package fr.miage.filestore.thumbnail;

public class ThumbnailBase64EncodingException extends RuntimeException {

    public ThumbnailBase64EncodingException(String message) {
        super(message);
    }

}
