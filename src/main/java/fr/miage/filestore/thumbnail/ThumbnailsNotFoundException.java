package fr.miage.filestore.thumbnail;

public class ThumbnailsNotFoundException extends Exception {

    public ThumbnailsNotFoundException(String message) {
        super(message);
    }

}
