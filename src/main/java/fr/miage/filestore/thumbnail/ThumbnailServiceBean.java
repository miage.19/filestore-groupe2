package fr.miage.filestore.thumbnail;

import fr.miage.filestore.store.BinaryStoreService;
import fr.miage.filestore.store.BinaryStreamNotFoundException;
import io.github.makbn.thumbnailer.AppSettings;
import io.github.makbn.thumbnailer.Thumbnailer;
import io.github.makbn.thumbnailer.ThumbnailerException;
import io.github.makbn.thumbnailer.exception.FileDoesNotExistException;
import io.github.makbn.thumbnailer.listener.ThumbnailListener;
import io.github.makbn.thumbnailer.model.ThumbnailCandidate;
import org.apache.commons.io.FileUtils;

import javax.ejb.EJB;
import javax.ejb.Singleton;
import java.io.File;
import java.io.IOException;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Base64;
import java.util.logging.Level;
import java.util.logging.Logger;

@Singleton
public class ThumbnailServiceBean implements ThumbnailService {

    @EJB
    private BinaryStoreService store;

    private static final Logger LOGGER = Logger.getLogger(ThumbnailServiceBean.class.getName());
    private final String dir = System.getenv("TEMP_DIR");

    public ThumbnailServiceBean() throws ThumbnailsNotFoundException {
        String[] args = new String[]{
                "-temp_dir", dir,
                "-thumb_height", System.getenv("THUMB_HEIGHT"),
                "-thumb_width", System.getenv("THUMB_WIDTH")
        };

        AppSettings.init(args);

        try {
            Thumbnailer.start();
        } catch (FileDoesNotExistException e) {
            throw new ThumbnailsNotFoundException("unable to retrieve existing thumbnails");
        }
    }

    @Override
    public void create(String key) {
        try {
            File in = new File(store.getPath(key).toUri());
            File out = new File(Paths.get(dir, key).toUri());
            Thumbnailer.createThumbnail(in, out);
        } catch (BinaryStreamNotFoundException | IOException | ThumbnailerException e) {
            throw new ThumbnailCreationFailedException("unable to create thumbnail.\n" + e.getMessage());
        }
    }

    @Override
    public String toBase64(String key) throws ThumbnailBase64EncodingException {
        Path path = Paths.get(dir, key);
        File thumbnail;
        byte[] content;

        try {
            thumbnail = new File(path.toUri());
            content = FileUtils.readFileToByteArray(thumbnail);
        } catch (IOException e) {
            throw new ThumbnailBase64EncodingException("unable to encode thumbnail to base64 (file not found)\n" + e.getMessage());
        }

        return Base64.getEncoder().encodeToString(content);
    }

}
